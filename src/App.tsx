import React from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './components/form/Person';
import Footer from './components/Footer';
import Header from './components/Header';
import { Grid, GridContainer } from '@trussworks/react-uswds';
import EmpoymentVerification from './components/EmploymentVerification';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <GridContainer>
          <Grid row>
            <Grid col={12}>
              <Header />
            </Grid>
          </Grid>

          <Grid row>
            <Grid col={12}>
              <main id="main-content" className="height-full">
                <EmpoymentVerification />
              </main>
            </Grid>
          </Grid>

          <Grid row>
            <Grid col={12}>
              <Footer />
            </Grid>
          </Grid>
        </GridContainer>
      </div>
    </BrowserRouter>
  );
}

export default App;
