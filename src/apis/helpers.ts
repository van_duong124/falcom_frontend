import { omit, some } from 'lodash';
import { IPersonForm } from '../components/form/Person/types';

export const isAllFillOut = (person: IPersonForm): boolean => {
  const newPerson = omit(person, 'caseId');
  return !some(newPerson, (prop) => prop == '');
};
