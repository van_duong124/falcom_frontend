import axios, { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';
import { useEffect, useState } from 'react';
import { IPersonForm } from '../components/form/Person/types';
import { IReviewApplicationRequest } from './types';
import { isAllFillOut } from './helpers';
import { IPerson } from '../components/types';
import { head, isEmpty } from 'lodash';

export const useGetApplication = (caseId: String, isSubmit: boolean) => {
  const [personForm, setPersonForm] = useState<IPersonForm | undefined>(
    undefined
  );

  useEffect(() => {
    if (caseId) {
      const url = `http://localhost:8080/verify/application?caseId=${caseId}`;
      (async () => {
        const headers: AxiosRequestHeaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        };

        const config: AxiosRequestConfig = {
          url: url,
          method: 'GET',
          headers: headers
        };

        const res = await axios.request(config);

        const { data } = res;

        if (data) setPersonForm(data);
      })();
    }
  }, [caseId, isSubmit]);

  return personForm;
};

export const useSubmitApplication = (req: IPersonForm | undefined) => {
  const [personForm, setPersonForm] = useState<IPersonForm | undefined>(
    undefined
  );

  useEffect(() => {
    const url = 'http://localhost:8080/verify/application/new';
    if (req && isAllFillOut(req)) {
      (async () => {
        const headers: AxiosRequestHeaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        };

        const config: AxiosRequestConfig = {
          url: url,
          method: 'POST',
          headers: headers,
          data: req
        };

        const res = await axios.request(config);

        const { data } = res;

        if (data) setPersonForm(data);
      })();
    }
  }, [req]);

  return personForm;
};

export const useReviewApplication = (req: IReviewApplicationRequest) => {
  const [personForm, setPersonForm] = useState<IPersonForm | undefined>(
    undefined
  );

  useEffect(() => {
    if (
      req &&
      !isEmpty(req.caseId) &&
      !isEmpty(req.caseStatus) &&
      req.isSubmit
    ) {
      const url = 'http://localhost:8080/verify/application/review';
      (async () => {
        const headers: AxiosRequestHeaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        };

        const config: AxiosRequestConfig = {
          url: url,
          method: 'POST',
          headers: headers,
          data: req
        };

        console.log(req);

        const res = await axios.request(config);

        const { data } = res;

        if (data) setPersonForm(data);
      })();
    }
  }, [req.caseId, req.caseStatus, req.isSubmit]);

  return personForm;
};

export const useDHSPerson = (req: IPersonForm | undefined) => {
  const [person, setPerson] = useState<IPerson | undefined>(undefined);

  useEffect(() => {
    if (req) {
      const url = 'http://localhost:8080/dhs/person';

      console.log(url);

      (async () => {
        const headers: AxiosRequestHeaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        };

        const config: AxiosRequestConfig = {
          url: url,
          method: 'GET',
          headers: headers,
          params: req
        };

        const res = await axios.request(config);

        const { data } = res;

        if (data) setPerson(head(data));
      })();
    }
  }, [req]);

  return person;
};
