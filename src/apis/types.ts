export interface IReviewApplicationRequest {
  caseId: string | undefined;
  caseStatus: string | undefined;
  isSubmit: boolean;
}
