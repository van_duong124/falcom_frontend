import {
  Alert,
  Button,
  Dropdown,
  Grid,
  GridContainer,
  Select,
  SummaryBox,
  SummaryBoxContent,
  SummaryBoxHeading,
  Table,
  Title
} from '@trussworks/react-uswds';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import {
  useDHSPerson,
  useGetApplication,
  useReviewApplication,
  useSubmitApplication
} from '../../apis/request';

const DECISIONS = [
  'reviewing',
  'employment_authorized',
  'employment_not_authorized',
  'record_mismatch',
  'close_case_and_resubmit'
];

export const AgentComp = () => {
  // // @ts-ignore
  const { caseId } = useParams() as any;
  const [isSubmit, setIsSubmit] = useState(false);

  const personForm = useGetApplication(caseId, isSubmit);
  const dhsPerson = useDHSPerson(personForm);

  const [decision, setDecision] = useState<string | undefined>();

  const app = useReviewApplication({ caseId, caseStatus: decision, isSubmit });

  return (
    <GridContainer className="margin-top-2 margin-bottom-2">
      {app && (
        <Alert
          type="success"
          headingLevel="h4"
          className="margin-bottom-2"
          heading="Submitted Successfully"
        >
          <div>Case ID: {app.caseId}</div>
          <div>Case Status: {app.caseStatus}</div>
        </Alert>
      )}

      <Grid row>
        <Title>Form I-9 - Person Info:</Title>

        <Grid col={12}>
          <div className="float-left">
            <h4>Case ID: {personForm?.caseId}</h4>
            <h4 className="float-left">
              Case Status: {personForm?.caseStatus}
            </h4>
          </div>
        </Grid>

        <Grid col={12}>
          <Table bordered={true} striped={true} scrollable>
            <thead>
              <tr>
                <th scope="col">Person Info</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Middle Name</th>
                <th scope="col">Date of Birth</th>
                <th scope="col">Social Security Number</th>

                <th scope="col">Gender</th>
                <th scope="col">Passport</th>

                <th scope="col">Street</th>
                <th scope="col">City</th>
                <th scope="col">State</th>
                <th scope="col">Zip Code</th>
              </tr>
            </thead>

            <tbody>
              <tr key="i9">
                {personForm && (
                  <>
                    <th>I-9 Form</th>
                    <td>{personForm.fname}</td>
                    <td>{personForm.lname}</td>
                    <td>{personForm.mname}</td>
                    <td>{personForm.dob}</td>
                    <td>{personForm.ssn}</td>

                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                  </>
                )}
              </tr>

              <tr key="dhs">
                {dhsPerson && (
                  <>
                    <th>DHS</th>
                    <td>{dhsPerson.fname}</td>
                    <td>{dhsPerson.lname}</td>
                    <td>{dhsPerson.mname}</td>
                    <td>{dhsPerson.dob}</td>
                    <td>{dhsPerson.ssn}</td>

                    <td>{dhsPerson.gender}</td>
                    <td>{dhsPerson.passport}</td>
                    <td>{dhsPerson.streetAddress}</td>
                    <td>{dhsPerson.cityOfResidence}</td>
                    <td>{dhsPerson.state}</td>
                    <td>{dhsPerson.zip}</td>
                  </>
                )}
              </tr>
            </tbody>
          </Table>
        </Grid>
      </Grid>

      <Grid row>
        <Grid col={12}>
          <Select
            id="decision_dropdown"
            name="decision_dropdown"
            className="margin-bottom-1 float-right"
            value={decision}
            onChange={(e) => setDecision(e.target.value)}
          >
            <option key="empty" value="">
              - Select -
            </option>
            {DECISIONS.map((d) => (
              <option key={d} value={d}>
                {d}
              </option>
            ))}
          </Select>
        </Grid>

        <Grid col={12}>
          <Button
            id="form-submit"
            type="submit"
            className="float-right margin-right-0"
            disabled={isSubmit}
            onClick={() => {
              setIsSubmit(true);
            }}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </GridContainer>
  );
};
