import { useParams } from 'react-router';
import { useGetApplication } from '../../apis/request';
import {
  Grid,
  GridContainer,
  Label,
  Table,
  Title
} from '@trussworks/react-uswds';

export const ApplicationComp = () => {
  // // @ts-ignore
  const { caseId } = useParams() as any;
  const personForm = useGetApplication(caseId, true);

  return (
    <GridContainer className="margin-top-2 margin-bottom-2">
      <Grid row>
        <Title>Form I-9 - Person Info:</Title>

        <Grid col={12}>
          <div className="float-left">
            <h4>Case ID: {personForm?.caseId}</h4>
            <h4 className="float-left">
              Case Status: {personForm?.caseStatus}
            </h4>
          </div>
        </Grid>

        <Grid col={12}>
          <Table bordered={true} striped={true} scrollable>
            <thead>
              <tr>
                <th scope="col">Person Info</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Middle Name</th>
                <th scope="col">Date of Birth</th>
                <th scope="col">Social Security Number</th>

                <th scope="col">Gender</th>
                <th scope="col">Passport</th>

                <th scope="col">Street</th>
                <th scope="col">City</th>
                <th scope="col">State</th>
                <th scope="col">Zip Code</th>
              </tr>
            </thead>

            <tbody>
              <tr key="i9">
                {personForm && (
                  <>
                    <th>I-9 Form</th>
                    <td>{personForm.fname}</td>
                    <td>{personForm.lname}</td>
                    <td>{personForm.mname}</td>
                    <td>{personForm.dob}</td>
                    <td>{personForm.ssn}</td>

                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                    <td>N/A</td>
                  </>
                )}
              </tr>
            </tbody>
          </Table>
        </Grid>
      </Grid>
    </GridContainer>
  );
};
