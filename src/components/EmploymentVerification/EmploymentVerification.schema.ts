import * as yup from 'yup';
import { PersonSchema } from '../form/Person';

const EmploymentVerificationSchema = yup.object().concat(PersonSchema);
