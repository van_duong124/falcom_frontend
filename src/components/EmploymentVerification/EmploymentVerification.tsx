import { BrowserRouter } from 'react-router-dom';
import Agent from '../Agent';
import Person from '../form/Person';
import { Route, Switch } from 'react-router';
import Application from '../Application';

export const EmpoymentVerificationComp = () => {
  return (
    <Switch>
      <Route exact={true} path="/">
        <Person />
      </Route>
      <Route exact={true} path="/application/review/:caseId">
        <Agent />
      </Route>
      <Route exact={true} path="/application/verify/:caseId">
        <Application />
      </Route>
    </Switch>
  );
};
