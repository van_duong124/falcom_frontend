import {
  Address,
  Footer,
  FooterNav,
  Logo,
  SocialLink,
  SocialLinks
} from '@trussworks/react-uswds';

import logoImg from '../../logo.svg';

export const FooterComp = () => {
  const socialLinkItems = [
    <SocialLink key="facebook" name="Facebook" href="#" />,
    <SocialLink key="twitter" name="Twitter" href="#" />,
    <SocialLink key="youtube" name="YouTube" href="#" />,
    <SocialLink key="instagram" name="Instagram" href="#" />,
    <SocialLink key="rss" name="RSS" href="#" />
  ];

  return (
    <Footer
      size="medium"
      primary={
        <FooterNav
          size="medium"
          links={Array(4).fill(
            <a className="usa-footer__primary-link" href="#">
              Primary Link
            </a>
          )}
        />
      }
      secondary={
        <div className="grid-row grid-gap">
          <Logo
            size="medium"
            image={
              <img
                className="usa-footer__logo-img"
                alt="img alt text"
                src={logoImg}
              />
            }
            heading={<p className="usa-footer__logo-heading">Name of Agency</p>}
          />
          <div className="usa-footer__contact-links mobile-lg:grid-col-6">
            <SocialLinks links={socialLinkItems} />
            <h3 className="usa-footer__contact-heading">
              Agency Contact Center
            </h3>
            <Address
              size="medium"
              items={[
                <a key="telephone" href="tel:1-800-555-5555">
                  (800) CALL-GOVT
                </a>,
                <a key="email" href="mailto:info@agency.gov">
                  info@agency.gov
                </a>
              ]}
            />
          </div>
        </div>
      }
    />
  );
};
