import {
  DatePicker,
  ErrorMessage,
  Grid,
  GridContainer,
  Label,
  TextInput
} from '@trussworks/react-uswds';
import { IFormState, IFormValidate } from '../../types';

import { format } from 'date-fns';
import { isEmpty } from 'lodash';

const getDate = (dob: string, formatType: string = 'yyyy-MM-dd') => {
  if (isEmpty(dob)) return '';
  const date = new Date(dob);
  return format(date, formatType) ?? '';
};

export const DateOfBirthComp = ({
  errors,
  touched,
  values,
  setValues
}: IFormState & IFormValidate) => {
  return (
    <GridContainer>
      <Grid row gap="md">
        <Grid col={4}>
          <Label htmlFor="dob">Date of Birth</Label>
          <DatePicker
            aria-describedby="date-of-birth"
            aria-labelledby="date-of-birth"
            id="dob"
            name="dob"
            placeholder="MM/DD/YYYY"
            formNoValidate={errors.dob && touched.dob ? true : false}
            defaultValue={getDate(values.dob, 'yyyy-MM-dd')}
            //@ts-ignore
            onChange={(val) =>
              setValues({ ...values, dob: getDate(val ?? '', 'yyyy-MM-dd') })
            }
          />
          {errors.dob && touched.dob && (
            <ErrorMessage>{errors.dob}</ErrorMessage>
          )}
        </Grid>
      </Grid>
    </GridContainer>
  );
};
