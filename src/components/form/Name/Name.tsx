import {
  ErrorMessage,
  Grid,
  GridContainer,
  Label,
  TextInput
} from '@trussworks/react-uswds';
import { IFormState, IFormValidate } from '../../types';

export const NameComp = ({
  errors,
  touched,
  values,
  setValues
}: IFormState & IFormValidate) => {
  return (
    <GridContainer>
      <Grid row gap="md">
        <Grid col={4}>
          <Label htmlFor="lname">Last Name</Label>
          <TextInput
            id="lname"
            name="lname"
            type="text"
            placeholder="Last Name"
            validationStatus={
              errors.lname && touched.lname ? 'error' : undefined
            }
            value={values.lname}
            onChange={(e) => setValues({ ...values, lname: e.target.value })}
          />
          {errors.lname && touched.lname && (
            <ErrorMessage>{errors.lname}</ErrorMessage>
          )}
        </Grid>

        <Grid col={4}>
          <Label htmlFor="fname">First Name</Label>
          <TextInput
            id="fname"
            name="fname"
            type="text"
            placeholder="First Name"
            validationStatus={
              errors.fname && touched.fname ? 'error' : undefined
            }
            value={values.fname}
            onChange={(e) => setValues({ ...values, fname: e.target.value })}
          />
          {errors.fname && touched.fname && (
            <ErrorMessage>{errors.fname}</ErrorMessage>
          )}
        </Grid>

        <Grid col={4}>
          <Label htmlFor="mname">Middle Name</Label>
          <TextInput
            id="mname"
            name="mname"
            type="text"
            placeholder="Middle Name"
            validationStatus={
              errors.mname && touched.mname ? 'error' : undefined
            }
            value={values.mname}
            onChange={(e) => setValues({ ...values, mname: e.target.value })}
          />
          {errors.mname && touched.mname && (
            <ErrorMessage>{errors.mname}</ErrorMessage>
          )}
        </Grid>
      </Grid>
    </GridContainer>
  );
};
