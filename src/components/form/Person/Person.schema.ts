import * as yup from 'yup';

const NAME_STRING_LENGTH = 50;

export const PersonSchema = yup.object().shape({
  caseId: yup.string().default('').notRequired(),
  fname: yup.string().default('').max(NAME_STRING_LENGTH).notRequired(),
  lname: yup.string().default('').max(NAME_STRING_LENGTH).notRequired(),
  mname: yup.string().default('').max(NAME_STRING_LENGTH).notRequired(),
  dob: yup // TODO: check valid date as string
    .string()
    .default('')
    .notRequired(),
  ssn: yup
    .string()
    .default('')
    .max(9) // TODO: length requirement for ssn
    .matches(/^[0-9]*$/, { excludeEmptyString: true })
    .notRequired(),
  caseStatus: yup.string().default('').notRequired()
});
