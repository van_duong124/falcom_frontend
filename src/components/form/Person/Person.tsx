import {
  Alert,
  Button,
  Form,
  Grid,
  GridContainer,
  Title
} from '@trussworks/react-uswds';
import Name from '../Name';
import { useCallback, useState } from 'react';
import DateOfBirth from '../DateOfBirth';
import SSN from '../SSN';
import { useFormik } from 'formik';
import { PersonSchema } from './Person.schema';
import { IPersonForm } from './types';
import { initFormValues } from './constants';
import { useSubmitApplication } from '../../../apis/request';

export const PersonComp = () => {
  const { values, errors, touched, setValues, handleSubmit, handleReset } =
    useFormik({
      initialValues: initFormValues,
      validationSchema: PersonSchema,
      onSubmit: (values) => {
        setFormValues({ ...values, caseStatus: 'reviewing' });
      },
      onReset: (values) => {
        setValues(initFormValues);
        setFormValues(initFormValues);
      }
    });

  const [formValues, setFormValues] = useState<IPersonForm>(values);

  const app = useSubmitApplication(formValues);

  return (
    <GridContainer>
      <Grid row>
        <Form onSubmit={handleSubmit}>
          {app && (
            <Alert
              type="success"
              headingLevel="h4"
              heading="Submitted Successfully"
            >
              <div>Case ID: {app.caseId}</div>
              <div>Case Status: {app.caseStatus}</div>
            </Alert>
          )}

          <Grid col={4}>
            <Title>Employee Information</Title>
          </Grid>

          <Grid col={12}>
            <Name
              values={values}
              errors={errors}
              touched={touched}
              setValues={setValues}
            />
          </Grid>

          <Grid col={12}>
            <DateOfBirth
              values={values}
              errors={errors}
              touched={touched}
              setValues={setValues}
            />
          </Grid>

          <Grid col={12}>
            <SSN
              values={values}
              errors={errors}
              touched={touched}
              setValues={setValues}
            />
          </Grid>

          <Grid col={3}>
            <Button id="form-submit" type="submit" disabled={false}>
              Submit
            </Button>
          </Grid>
        </Form>
      </Grid>
    </GridContainer>
  );
};
