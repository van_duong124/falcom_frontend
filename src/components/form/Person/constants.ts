import { IPersonForm } from './types';

export const initFormValues: IPersonForm = {
  caseId: '',
  fname: '',
  lname: '',
  mname: '',
  dob: '',
  ssn: '',
  caseStatus: ''
};
