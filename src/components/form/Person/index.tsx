import { PersonComp as Person } from './Person';
import { PersonSchema } from './Person.schema';
import { IPersonForm } from './types';

export { PersonSchema };
export default Person;
