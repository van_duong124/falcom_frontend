export interface IPersonForm {
  caseId: string;
  fname: string;
  lname: string;
  mname: string;
  dob: string;
  ssn: string;
  caseStatus: string;
}
