import {
  DatePicker,
  ErrorMessage,
  Grid,
  GridContainer,
  Label,
  TextInput
} from '@trussworks/react-uswds';
import { IFormState, IFormValidate } from '../../types';

export const SSNComp = ({
  errors,
  touched,
  values,
  setValues
}: IFormState & IFormValidate) => {
  return (
    <GridContainer>
      <Grid row gap="md">
        <Grid col={4}>
          <Label htmlFor="ssn">Social Security Number</Label>
          <TextInput
            id="ssn"
            name="ssn"
            type="text"
            placeholder="Social Security Number"
            validationStatus={errors.ssn && touched.ssn ? 'error' : undefined}
            value={values.ssn}
            onChange={(e) => setValues({ ...values, ssn: e.target.value })}
          />
          {errors.ssn && touched.ssn && (
            <ErrorMessage>{errors.fname}</ErrorMessage>
          )}
        </Grid>
      </Grid>
    </GridContainer>
  );
};
