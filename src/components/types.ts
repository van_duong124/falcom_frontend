import { FormikErrors, FormikTouched } from 'formik';
import { IPersonForm } from './form/Person/types';

export interface IFormState {
  values: IPersonForm;
  setValues: (
    values: React.SetStateAction<IPersonForm>,
    shouldValidate?: boolean | undefined
  ) => Promise<FormikErrors<IPersonForm>> | Promise<void>;
}

export interface IFormValidate {
  errors: FormikErrors<IPersonForm>;
  touched: FormikTouched<IPersonForm>;
}

export interface IPerson {
  personId: string;
  fname: string;
  lname: string;
  mname: string;
  gender: string;
  dob: string;
  passport: string;
  ssn: string;
  streetAddress: string;
  cityOfResidence: string;
  state: string;
  zip: string;
}
